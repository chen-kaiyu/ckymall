import {createApp} from 'vue'
import toast from './toast.vue'

const plugin={}

plugin.install=function(app,option){  
  const vm=createApp(toast)
  const container =vm.mount(document.createElement('div'))
  document.body.appendChild(container.$el)

  app.config.globalProperties.$Toast = function(content = "未命名", delTime = "2000") {
    container.message = content;
    container.isShow = true;
    setTimeout(() => {
      container.isShow = false;
    }, delTime);
  }
 }
 export default plugin

// export function show(paramcontent,paramdelTime) {
//   toast.methods.controlShow(paramcontent,paramdelTime)
// }
