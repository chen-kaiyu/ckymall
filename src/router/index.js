import { createRouter, createWebHistory } from 'vue-router'
const Home=()=>import("../views/home/Home.vue")
const Cart=()=>import("../views/cart/Cart.vue")
const Category=()=>import("../views/category/Category.vue")
const Profile=()=>import("../views/profile/Profile.vue")
const Detail=()=>import("../views/detail/Detail.vue")

const routes = [
  {
    path:"",
    meta:{
      keepAlive: true
    },
    redirect: "/home"
  },
  {
    path:'/home',
    name:'Home',
    id:1,
    meta:{
      keepAlive: true
    },
    component:Home
  },
  {
    path:'/cart',
    name:"Cart",
    meta:{
      keepAlive: true
    },
    id:2,
    component:Cart
  },
  {
    path:"/category",
    name:"Category",
    id:3,
    meta:{
      keepAlive: true
    },
    component:Category
  },
  {
    path:'/profile',
    name:"Profile",
    id:4,
    meta:{
      keepAlive: true
    },
    component:Profile
  },
  {
    path:"/detail",
    name:"Detail",
    id:5,
    meta:{
      keepAlive:false
    },
    component:Detail
  } 
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
