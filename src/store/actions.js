import {ADD_COUNT,ADD_TO_CART} from './mutation-types'

export default {
  //context={commit,state}

  async addCart(context,payload){
    let oldproduct=context.state.cartList.find(item=>item.iid==payload.iid)
    if(oldproduct){
    // oldproduct.count+=1
    context.commit(ADD_COUNT,oldproduct)
    return '商品数量+1';
    }else{
      payload.count=1
      context.commit(ADD_TO_CART,payload)
      return '添加一个商品'
    }
    //通过遍历判断商品的iid是否相同
    // let oldproduct=null
    // for (let item of state.cartList) {
    //   if(item.iid=payload.iid){
    //     oldproduct=item
    //     break
    //   }
    // }
    
    //通过数组find（）方法查找,find返回第一个符合的数据
    // return new Promise((resolve, reject) => {
    // let oldproduct=context.state.cartList.find(item=>item.iid==payload.iid)
    // if(oldproduct){
    //   // oldproduct.count+=1
    //   context.commit(ADD_COUNT,oldproduct)
    //   resolve('商品数量+1')
    // }else{
    //   payload.count=1
    //   context.commit(ADD_TO_CART,payload)
    //   resolve('添加一个商品')
    // }
    // })
  }
}