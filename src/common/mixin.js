import BackTop from "components/content/backtop/BackTop";

export const BackTopMix={
  components:{
    BackTop
  },
  data() {
    return {
      isbackTop: false,
    }
  },
  methods: {
    backtop() {
      this.$refs.scroll.scrollTo(0, 0, 500);
    },
  },
}