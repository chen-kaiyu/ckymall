import {request} from './request'

export function getDetaildata(iid){
  return request({
    url:"/detail",
    params:{
      iid
    }
  })
}

export function getRecommend(){
  return request({
    url:"/recommend"
  })
}

export class getBaseInfo{
  constructor(itemInfo,columns,services){
    this.title=itemInfo.title
    this.oldPrice=itemInfo.oldPrice
    this.price=itemInfo.price
    this.discountDesc=itemInfo.discountDesc
    this.discountBgColor=itemInfo.discountBgColor
    this.columns=columns
    this.services=services
    this.realPrice = itemInfo.lowNowPrice	
  }
}

export class getShopInfo{
  constructor(shopInfo){
    this.shopLogo=shopInfo.shopLogo
    this.score=shopInfo.score
    this.name=shopInfo.name
    this.fans=shopInfo.cFans
    this.sells=shopInfo.cSells
    this.goodCount=shopInfo.cGoods
  }
}

export class getItemParams{
  constructor(info,rule){
    this.image=info.images ?info.images[0]:''
    this.info=info.set
    this.rule=rule.tables
  }
}