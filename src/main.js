import mitt from 'mitt'
import { createApp, inject } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import toast from 'components/content/toast/index';
import VueLazyLoad from 'vue3-lazyload'
import fastclick from 'fastclick';

const app= createApp(App)
app.use(store).use(router).use(toast)
app.use(VueLazyLoad, {
  loading: require("assets/img/common/placeholder.png")
})
app.mount('#app')

app.config.globalProperties.$bus=mitt() 

// provide,inject,定义全局方法
// app.provide('$showToast',show)

//解决移动端300s延迟
fastclick.attach(document.body)

