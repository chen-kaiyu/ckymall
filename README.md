# ckymall



#### 项目描述：

学习时做的一款女装销售的商城系统，移动端h5页面，商品种类众多，进入详情页可加入购物车

#### 项目特点：

1. 基于vue3框架，SPA单页面应用
2. 模块化，公共的数据请求组件和功能组件等，基于vue3封装toast插件
3. 页面滑动使用Better scroll
4. vuex状态管理，存储购物车信息
5. 移动端事件touchstart、touchmove和touchend实现轮播图滚动
6. 简单优化用户体验，移动端300s点击延迟，防抖/节流函数，vue3-lazyload图片懒加载



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
